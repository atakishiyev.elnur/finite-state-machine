/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.state.machine.test;

import java.util.logging.Level;
import java.util.logging.Logger;

import dev.ocean.state.machines.*;
import org.junit.Before;
import org.junit.Test;

/**
 * @author eatakishiyev
 */
public class FsmTest {
    FiniteStateMachine fsm = new FiniteStateMachine("TEST_FSM");

    @Before
    public void init() throws Exception {
        State stateA = fsm.createState("STATE_A");
        State stateB = fsm.createState("STATE_B").setStateTimeout(4000, new TimeOutHandler() {
            @Override
            public void onTimeOut() {

                System.out.println("Time out on state A");
                try {
                    fsm.doTransition("MOVE_C");
                } catch (Exception ex) {
                    Logger.getLogger(FsmTest.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).setEntryAction(new StateHandler() {
            public void execute() throws Exception {
                System.out.println("Entered to STATE_B");
            }
        });
        State stateC = fsm.createState("STATE_C").setEntryAction(new StateHandler() {
            public void execute() throws Exception {
                System.out.println("Entered to STATE_C");
                fsm.doTransition("MOVE_D");
            }
        });

        State stateD = fsm.createState("STATE_D");
        State stateE = fsm.createState("STATE_E").setEntryAction(new StateHandler() {
            public void execute() throws Exception {
                System.out.println("Entered to STATE_E");
            }
        });

        stateA.createTransition("MOVE_B", stateB);
        stateA.createTransition("MOVE_C", stateC);
        stateB.createTransition("MOVE_C", stateC);
        stateC.createTransition("MOVE_D", stateD).setAfterTransitionHandler(new TransitionHandler() {
            public boolean execute() throws Exception {
                fsm.doTransition("MOVE_E");
                return true;
            }
        });
        stateD.createTransition("MOVE_E", stateE);

        fsm.setStart(stateA);

    }

    @Test(expected = Exception.class)
    public void unexpectedTransitionTest() throws Exception {
        fsm.doTransition("MOVE_E");
    }

    @Test
    public void expectedTransitionTest() throws Exception {
        fsm.doTransition("MOVE_B");
    }
}
