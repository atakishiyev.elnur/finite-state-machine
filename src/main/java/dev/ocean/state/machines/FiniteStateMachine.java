/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.state.machines;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

/**
 *
 * @author eatakishiyev
 */
/*
 * Finite State Machine to change state of object. Object must implement
 * Transitible interface
 */
public class FiniteStateMachine implements Serializable {

    private String name;
    protected State currentState;
    protected State previousState;
    private State start;
    protected List<State> states = new ArrayList();
    private transient Object attachment;
    protected transient ScheduledExecutorService scheduledExecutorService;
    protected transient Future future;

    public FiniteStateMachine(String name) {
        this();
        this.name = name;
    }

    public FiniteStateMachine() {
        this.scheduledExecutorService = Executors.newScheduledThreadPool(2);
    }

    /**
     * Creates new State.
     *
     * @return
     * @throws java.lang.Exception
     * @see State
     * @param name new State name.
     *
     */
    public State createState(String name) throws Exception {
        for (int i = 0; i < this.states.size(); i++) {
            State state = this.states.get(i);
            if (state.getName().equals(name)) {
                throw new Exception(String.format("State %s already exists.", name));
            }
        }
        State state = new State(name, this);
        states.add(state);
        return state;
    }

    public State removeState(String name) throws Exception {
        for (int i = 0; i < this.states.size(); i++) {
            State state = this.states.get(i);
            if (state.getName().equals(name)) {
                this.states.remove(state);
                return state;
            }
        }
        throw new Exception(String.format("State %s is not exists.", name));
    }

    public void doTransition(String condition) throws Exception {

        Transition transition = this.currentState.getTransition(condition);

        if (transition == null) {
            throw new Exception(String.format("Transition not exists %s. State: %s", condition, this.getCurrentState().getName()));
        }

        if (!(future == null)) {
            future.cancel(true);
            future = null;
        }

        transition.perform(this);
    }

    protected void cancelTransition() {
        this.currentState = this.previousState;
    }

    public void attach(Object attachment) {
        this.attachment = attachment;
    }

    public Object attachment() {
        return this.attachment;
    }

    /**
     * @return the stateCurrent
     */
    public State getCurrentState() {
        return currentState;
    }

    protected void setCurrentState(State currentState) {
        this.currentState = currentState;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param start the start to set
     */
    public void setStart(State start) {
        this.start = start;
        this.currentState = start;
    }
}
