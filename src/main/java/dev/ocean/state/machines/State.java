/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.state.machines;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author eatakishiyev
 */
public class State implements Serializable {

    private String name;
    private transient StateHandler entryAction;
    private transient StateHandler exitAction;
    private transient TimeOutHandler timeOutHandler;
    private long timeout;
    private transient List<Transition> transitions = new ArrayList();
    private FiniteStateMachine fsm = null;

    protected State(String name, FiniteStateMachine fsm) {
        this.name = name;
        this.fsm = fsm;
        
    }

    /**
     * @return the state
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the state to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public Transition getTransition(String condition) {
        for (int i = 0; i < this.transitions.size(); i++) {
            Transition transition = this.transitions.get(i);
            if (transition.getTransitionCondition().equals(condition)) {
                return transition;
            }
        }
        return null;
    }

    /**
     * @return the exitAction
     */
    public StateHandler getExitAction() {
        return exitAction;
    }

    protected void enter() throws Exception {
        if (entryAction != null) {
            entryAction.execute();
        }
        if (timeOutHandler != null) {
            timeOutHandler.fsm = fsm;
            this.fsm.future = this.fsm.scheduledExecutorService.schedule(timeOutHandler, timeout, TimeUnit.MILLISECONDS);
        }
    }

    protected void exit() throws Exception {
        if (exitAction != null) {
            exitAction.execute();
        }
    }

    /**
     * @param exitAction the exitAction to set
     */
    public State setExitAction(StateHandler exitAction) {
        this.exitAction = exitAction;
        return this;
    }

    /**
     * @return the entryAction
     */
    public StateHandler getEntryAction() {
        return entryAction;
    }

    /**
     * @param entryAction the entryAction to set
     */
    public State setEntryAction(StateHandler entryAction) {
        this.entryAction = entryAction;
        return this;
    }

    public State setStateTimeout(long timeout, TimeOutHandler timeOutHandler) {
        this.timeOutHandler = timeOutHandler;
        this.timeout = timeout;
        return this;
    }

    public Transition createTransition(String condition, State to) throws Exception {
        Transition transition = new Transition(condition, to);
        this.transitions.add(transition);
        return transition;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final State other = (State) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.name);
        return hash;
    }
}
