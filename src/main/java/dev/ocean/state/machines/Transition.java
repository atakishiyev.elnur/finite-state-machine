/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.state.machines;

/**
 *
 * @author eatakishiyev
 */
public class Transition {

    private TransitionHandler transitionHandler;
    private TransitionHandler afterTransitionHandler;
    private String transitionCondition;
    private State transiteTo;

    protected Transition(String transitionCondition, State transiteTo) {
        this.transitionCondition = transitionCondition;
        this.transiteTo = transiteTo;
    }

    protected void perform(FiniteStateMachine fsm) throws Exception {

        if (transitionHandler != null) {
            if (!transitionHandler.execute()) {
                return;
            }
        }
        fsm.currentState.exit();

        fsm.previousState = fsm.currentState;
        fsm.currentState = transiteTo;


        transiteTo.enter();

        if (afterTransitionHandler != null) {
            afterTransitionHandler.execute();
        }

    }

    /**
     * @return the transitionCondition
     */
    public String getTransitionCondition() {
        return transitionCondition;
    }

    /**
     * @param transitionCondition the transitionCondition to set
     */
    public void setTransitionCondition(String transitionCondition) {
        this.transitionCondition = transitionCondition;
    }

    /**
     * @return the toState
     */
    public State getTransiteTo() {
        return transiteTo;
    }

    /**
     * @param transiteTo the toState to set
     */
    protected void setTransiteTo(State transiteTo) {
        this.transiteTo = transiteTo;
    }

    /**
     * @return the transitionHandler
     */
    public TransitionHandler getTransitionHandler() {
        return transitionHandler;
    }

    /**
     * @param transitionHandler the transitionHandler to set
     */
    public void setTransitionHandler(TransitionHandler transitionHandler) {
        this.transitionHandler = transitionHandler;
    }

    /**
     * @return the afterTransitionHandler
     */
    public TransitionHandler getAfterTransitionHandler() {
        return afterTransitionHandler;
    }

    /**
     * @param afterTransitionHandler the afterTransitionHandler to set
     */
    public void setAfterTransitionHandler(TransitionHandler afterTransitionHandler) {
        this.afterTransitionHandler = afterTransitionHandler;
    }
}
