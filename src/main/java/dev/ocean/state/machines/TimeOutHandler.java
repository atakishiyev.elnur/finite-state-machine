/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.state.machines;

/**
 *
 * @author eatakishiyev
 */
public abstract class TimeOutHandler implements Runnable {

    protected FiniteStateMachine fsm;

    @Override
    public final void run() {

        fsm.cancelTransition();
        onTimeOut();

    }

    public abstract void onTimeOut();
}
