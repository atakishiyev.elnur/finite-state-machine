/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.state.machines;

/**
 *
 * @author eatakishiyev
 */
public interface TransitionHandler {

    public boolean execute() throws Exception;
}
